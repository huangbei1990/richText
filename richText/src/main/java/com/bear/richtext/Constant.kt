package com.bear.richtext

object Constant {
    const val Default_MARGIN = 5 //默认组件间隔

    const val DEFAULT_TEXTSIZE = 20f

    object Event {
        val IMG_LOAD_ERROR = "load_img_error" //图片加载错误
        val PARSE_ITEM_ERROR = "parse_item_error" //解析错误
        val EXPOSURE = "exposure" //曝光
        val UNEXPOSURE = "unExposure" //反曝光
    }
}