package com.bear.richtext.utils

/**
 * 日志接口，方便外部调用者获取到日志
 */
interface IRichTextLog {
    fun vlog(tag: String, content: String)
    fun ilog(tag: String, content: String)
    fun dlog(tag: String, content: String)
    fun wlog(tag: String, content: String)
    fun eLog(tag: String, content: String)
}