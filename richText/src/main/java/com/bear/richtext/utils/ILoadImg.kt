package com.bear.richtext.utils

import android.content.Context
import android.widget.ImageView

/**
 * 图片加载接口
 */
interface ILoadImg {
    fun loadImg(context: Context, url: String, view: ImageView)
}