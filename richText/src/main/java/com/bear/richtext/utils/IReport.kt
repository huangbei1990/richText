package com.bear.richtext.utils

interface IReport {
    fun report(event: String, params: Map<String, String>)
}