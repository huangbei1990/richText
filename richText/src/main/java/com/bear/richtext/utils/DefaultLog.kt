package com.bear.richtext.utils

import android.util.Log

/**
 * 日志接口默认实现，若外部想获取内部的日志，则需要实现IRichTextLog接口
 */
class DefaultLog : IRichTextLog {
    override fun vlog(tag: String, content: String) {
        Log.v(tag, content)
    }

    override fun ilog(tag: String, content: String) {
        Log.i(tag, content)
    }

    override fun dlog(tag: String, content: String) {
        Log.d(tag, content)
    }

    override fun wlog(tag: String, content: String) {
        Log.w(tag, content)
    }

    override fun eLog(tag: String, content: String) {
        Log.e(tag, content)
    }
}