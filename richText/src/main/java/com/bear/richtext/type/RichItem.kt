package com.bear.richtext.type

import android.content.Context
import android.view.View
import org.json.JSONObject

abstract open class RichItem {

    protected var richView: View? = null

    abstract fun toJson(): JSONObject
    abstract fun parse(context: Context, jsonObject: JSONObject): View
    abstract fun type(): RichItemType
    open fun getView(): View? {
        return this.richView
    }

    //设置是否可编辑
    open fun setEditable(editable:Boolean){

    }

    companion object {
        val TYPE = "type"
        val ATTRS = "attrs"
    }
}