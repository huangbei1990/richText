package com.bear.richtext.type

//富文本组件类型
enum class RichItemType {
    Image, Text, Gif, Tag, Clickable, NewLine
}