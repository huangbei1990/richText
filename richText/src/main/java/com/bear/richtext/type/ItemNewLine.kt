package com.bear.richtext.type

import android.content.Context
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import org.json.JSONObject

/**
 * 换行组件
 */
class ItemNewLine() : RichItem() {

    companion object {
        const val ROWS = "rows" //换行的行数
        const val DEFAULT_LINE_HEIGHT = 10 //默认的行高
    }

    private var rows = 0

    constructor(context: Context, jsonObject: JSONObject) : this() {
        this.richView = parse(context, jsonObject)
    }

    override fun toJson(): JSONObject {
        var jsonObject = JSONObject()
        jsonObject.put(TYPE, RichItemType.NewLine)
        var attrs = JSONObject()
        attrs.put(ROWS, rows)
        jsonObject.put(ATTRS, attrs)
        return jsonObject
    }

    override fun parse(context: Context, jsonObject: JSONObject): View {
        val view = View(context)
        this.rows = jsonObject.optJSONObject(ATTRS).optInt(ROWS, 0)
        val layoutParams = LinearLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        )
        layoutParams.height = rows * DEFAULT_LINE_HEIGHT
        view.layoutParams = layoutParams
        this.richView = view
        return view
    }

    override fun type(): RichItemType {
        return RichItemType.NewLine
    }
}