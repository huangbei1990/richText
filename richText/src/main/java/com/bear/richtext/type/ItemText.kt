package com.bear.richtext.type

import android.content.Context
import android.graphics.Color
import android.widget.EditText
import android.widget.FrameLayout
import androidx.core.view.setMargins
import com.bear.richtext.Constant
import com.bear.richtext.utils.Tools
import org.json.JSONObject

/**
 * 正文组件
 */
class ItemText() : RichItem() {
    companion object {
        const val TEXT_SIZE = "textSize" //文字大小
        const val TEXT_BOLD = "textBold" //是否加粗
        const val TEXT_COLOR = "textColor" //文字颜色
        const val CONTENT = "content" //文本内容

        const val DEFAULT_TEXTSIZE = 20 //默认文字大小
        const val DEFAULT_TEXTCOLOR = "#616161" //默认文字颜色

        const val TAG = "ItemText"
    }

    private var textBold = false
    private var textColor = DEFAULT_TEXTCOLOR

    override fun toJson(): JSONObject {
        (this.richView as EditText)?.let {
            var jsonObject = JSONObject()
            jsonObject.put(TYPE, RichItemType.Text)
            var attrs = JSONObject()
            attrs.put(CONTENT, it.text.toString())
            attrs.put(TEXT_SIZE, it.textSize)
            attrs.put(TEXT_BOLD, textBold)
            attrs.put(TEXT_COLOR, textColor)
            jsonObject.put(ATTRS, attrs)
            return jsonObject
        }
        return JSONObject()
    }

    constructor(context: Context) : this() {
        this.richView = parse(context, JSONObject())
    }

    constructor(context: Context, jsonObject: JSONObject) : this() {
        this.richView = parse(context, jsonObject)
    }

    override fun parse(context: Context, jsonObject: JSONObject): EditText {
        val editText = EditText(context)
        val layoutParams = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.WRAP_CONTENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        )
        layoutParams.setMargins(Constant.Default_MARGIN)
        editText.layoutParams = layoutParams
        editText.isCursorVisible = true
        editText.background = null

        try {
            //属性设置
            jsonObject.optJSONObject(ATTRS)?.let {
                editText.textSize = it.optInt(TEXT_SIZE, DEFAULT_TEXTSIZE)?.toFloat()
                editText.setText(it.optString(CONTENT, ""))
                this.textBold = it.optBoolean(TEXT_BOLD, textBold)
                editText.paint.isFakeBoldText = textBold
                this.textColor = it.optString(TEXT_COLOR, DEFAULT_TEXTCOLOR)
                editText.setTextColor(Color.parseColor(textColor))
            }
        } catch (e: Exception) {
            Tools.elog(TAG, "parse itemtext error:${e}")
        }
        return editText
    }

    override fun type(): RichItemType {
        return RichItemType.Text
    }

    override fun setEditable(editable: Boolean) {
        this.richView?.let {
            (it as EditText).apply {
                if (!editable) {
                    this.setFocusable(false)
                    this.isFocusableInTouchMode = false
                } else {
                    this.setFocusable(true)
                    this.isFocusableInTouchMode = true
                }
            }
        }
    }
}